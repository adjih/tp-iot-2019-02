#! /bin/bash -x
# C.A. 2018 - 2022

#RIOTVER=2022.01
RIOTVER=2019.07
#RIOTVER=2017.10
CHANNEL=$1
PAN_ID=$2

WITH_MAKEFILE_PAN_ID=true
#WITH_MAKEFILE_PAN_ID=false

BAUDRATE=115200
#BAUDRATE=500000

#---------------------------------------------------------------------------

if [ `hostname` != saclay ] ; then
    echo "Error: script should be run on an saclay.iot-lab.info" ; exit 1
fi

if [ -z "$CHANNEL" ] ; then
    echo "Error: syntax: $0 <channel> <pan-id>" ; exit 1
fi

if [ -z "$PAN_ID" ] ; then
    echo "Error: syntax: $0 <channel> <pan-id>" ; exit 1
fi

#---------------------------------------------------------------------------

#	     && echo "CFLAGS += -DETHOS_BAUDRATE=$(ETHOS_BAUDRATE)" \
#		     >> Makefile.panid \

BR_DIR=RIOT-${RIOTVER}/examples/gnrc_border_router

create_makefile_pan_id() {
    if ${WITH_MAKEFILE_PAN_ID} ; then    
	test -e  ${BR_DIR}/Makefile.panid && return 0
	(cd ${BR_DIR} && cp -av Makefile Makefile.panid  \
	     && echo "CFLAGS += -DIEEE802154_DEFAULT_PANID=${PAN_ID}" \
		     >> Makefile.panid \
	) && return 0
    else
	rm -f ${BR_DIR}/Makefile.panid
	ln -s Makefile ${BR_DIR}/Makefile.panid || return 1
    fi
}

#---------------------------------------------------------------------------

cd ~/A8 || exit 1

NAME=gnrc_border_router_riot${RIOTVER}_ch${CHANNEL}_pan${PAN_ID}.elf

# If not here: git clone RIOT version 2017.10
#    and put it in ~/A8/RIOT-2017-10
test -e RIOT-${RIOTVER} || {
    git clone http://github.com/RIOT-OS/RIOT RIOT-${RIOTVER} -b ${RIOTVER}-branch
    rm -f ${NAME}
} || exit 1

# If not here: compile RIOT border_router for iotlab-a8-m3 with correct channel
#    and put it in ~/A8/gnrc_border_router_ch<channel>.elf

test -e ${NAME} || (
    source /opt/riot.source || exit 1
    create_makefile_pan_id || exit 1
    cd ${BR_DIR} \
	&& make clean all DEFAULT_CHANNEL=${CHANNEL} DEFAULT_PAN_ID=${PAN_ID} \
		BOARD=iotlab-a8-m3 ETHOS_BAUDRATE=${BAUDRATE} \
		-f Makefile.panid QUIET=0 \
       && mv bin/iotlab-a8-m3/gnrc_border_router.elf \
	      ~/A8/${NAME} || exit 1
) || exit 1
