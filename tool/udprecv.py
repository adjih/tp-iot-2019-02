#! /usr/bin/env python

import socket
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("--port", type=int, default=61617,
                    help="destination port")
args = parser.parse_args()

sd = socket.socket(socket.AF_INET6, socket.SOCK_DGRAM)
sd.bind(("", args.port))
print("Listening UDP packets on port %s" % args.port)

MAX_PACKET_SIZE = 100000
while True:
    packet, address = sd.recvfrom(MAX_PACKET_SIZE)
    print("received packet:", repr(packet), "from:", address)
