#! /bin/bash -x
# C.A. 2018-2022

#RIOTVER=2022.01
RIOTVER=2019.07
#RIOTVER=2017.10
CHANNEL=$1
PAN_ID=$2

BAUDRATE=115200
#BAUDRATE=500000

if [ `uname -m` != armv7l ] ; then
    echo "Error: script should be run on an A8 node" ; exit 1
fi

if [ -z "$CHANNEL" ] ; then
    echo "Error: syntax: $0 <channel> <pan-id>" ; exit 1
fi

if [ -z "$PAN_ID" ] ; then
    echo "Error: syntax: $0 <channel> <pan-id>" ; exit 1
fi

#---------------------------------------------------------------------------

ETHOS_DIR=RIOT-${RIOTVER}/dist/tools/ethos

# update_ethos_baudrate() {
#     test -e  ${ETHOS_DIR}/start_network_baudrate.sh && return 0
#     (cd ${ETHOS_DIR} \
# 	    && sed s/115200/${BAUDRATE}/g < start_network.sh \
# 		   > start_network_baudrate.sh \
# 		&& chmod a+x start_network_baudrate.sh \
# 		       && return 0)
#     return 0
# }

#---------------------------------------------------------------------------

cd /home/root/A8 || exit 1

NAME=gnrc_border_router_riot${RIOTVER}_ch${CHANNEL}_pan${PAN_ID}.elf
test -e ${NAME} || {
  echo "No firmware for border router in ~/A8 (${NAME})"
  exit 1
}

#
#source /opt/riot.source

test -e RIOT-${RIOTVER} || {
    git clone http://github.com/RIOT-OS/RIOT RIOT-${RIOTVER} -b ${RIOTVER}-branch
} || exit 1


test -e RIOT-${RIOTVER}/dist/tools/uhcpd/bin/uhcpd  || (
    cd RIOT-${RIOTVER}/dist/tools/uhcpd && make 
) || exit 1

#update_ethos_baudrate || exit 1

test -e RIOT-${RIOTVER}/dist/tools/ethos/bin/ethos  || (
    cd RIOT-${RIOTVER}/dist/tools/ethos && make
) || exit 1

if [ "z$2" != "z--no-flash" ] ; then
    flash_a8_m3 ${NAME} || exit 1
else
    reset_a8_m3
fi

cd ${ETHOS_DIR}  \
    && ./start_network.sh /dev/ttyA8_M3 \
			  tap0 ${INET6_PREFIX}::/64 ${BAUDRATE} 

