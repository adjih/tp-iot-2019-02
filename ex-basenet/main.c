/* Example of C program */
#include <stdio.h>

int main(void)
{
    printf("Example RIOT program ["__TIME__"]\n");
    printf("Radio channel: %d\n", CONFIG_IEEE802154_DEFAULT_CHANNEL);    
    printf("PAN ID: 0x%04x\n", CONFIG_IEEE802154_DEFAULT_PANID);        
}
